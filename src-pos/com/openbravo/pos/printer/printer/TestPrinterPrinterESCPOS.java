package com.openbravo.pos.printer.printer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;

public class TestPrinterPrinterESCPOS {

	public static void main(String[] args) {
		try {
			// Find the default service
			DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;

			PrintService service = PrintServiceLookup.lookupDefaultPrintService();

			// Create the print job
			DocPrintJob job = service.createPrintJob();
			InputStream in = new FileInputStream("/home/david/openbravo/testescpos.print");
			Doc docNew = new SimpleDoc(in, flavor, null);

			// Monitor print job events; for the implementation of
			// PrintJobWatcher,
			// see Determining When a Print Job Has Finished
			PrintJobWatcher pjDone = new PrintJobWatcher(job);

			// Print it
			job.print(docNew, null);

			// Wait for the print job to be done
			pjDone.waitForDone();

			// It is now safe to close the input stream
			in.close();
		} catch (Exception e) {
		
			e.printStackTrace();
		}

	}

}

class PrintJobWatcher {

    // true iff it is safe to close the print job's input stream
    private boolean done = false;

    /**
     * @param job
     */
    public PrintJobWatcher(DocPrintJob job) {
            // Add a listener to the print job
            job.addPrintJobListener(new PrintJobAdapter() {
                    public void printJobCanceled(PrintJobEvent pje) {
                            allDone();
                    }

                    public void printJobCompleted(PrintJobEvent pje) {
                            allDone();
                    }

                    public void printJobFailed(PrintJobEvent pje) {
                            allDone();
                    }

                    public void printJobNoMoreEvents(PrintJobEvent pje) {
                            allDone();
                    }

                    void allDone() {
                            synchronized (PrintJobWatcher.this) {
                                    done = true;
                                    PrintJobWatcher.this.notify();
                            }
                    }
            });
    }

    /**
     * Blocks on waiting for the print job...
     */
    public synchronized void waitForDone() {
            try {
                    System.out.println("Checking Printer Status...");
                    while (!done) {
                            System.out.println(" Waiting for the Print Job to Complete...");
                            wait();
                    }
                    System.out.println("Print Job Completed.");
            } catch (InterruptedException e) {
            }
    }
}