//    Openbravo POS is a point of sales application designed for touch screens.
//    Copyright (C) 2007-2009 Openbravo, S.L.
//    http://www.openbravo.com/product/pos
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package com.openbravo.pos.printer.escpos;


import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URI;

public class PrinterWritterSocket extends PrinterWritter {
    
    private URI m_uriFilePrinter;
    private Socket m_sockPrinter;
    private OutputStream m_out;
    
    public PrinterWritterSocket(URI uriFilePrinter) {
    	m_uriFilePrinter = uriFilePrinter;
        m_out = null;
    }  
    
    protected void internalWrite(byte[] data) {
        try {  
            if (m_out == null) {
            	m_sockPrinter=new Socket(m_uriFilePrinter.getHost(), m_uriFilePrinter.getPort());
            	m_out = m_sockPrinter.getOutputStream();
            }
            m_out.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }    
    }
    
    protected void internalFlush() {
        try {  
            if (m_out != null) {
                m_out.flush();                
            }
        } catch (IOException e) {
        	e.printStackTrace();
        }    
    }
    
    protected void internalClose() {
        try {  
            if (m_out != null) {
                m_out.flush();
                m_out.close();
                m_sockPrinter.close();
                m_out = null;
            }
        } catch (IOException e) {
        	e.printStackTrace();
        }    
    }    
}
